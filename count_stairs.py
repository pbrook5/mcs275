def count_stairs(n):
	"""
	Print the number of ways one can achieve the star counts. An eye is kept on the door clean.
	Preconditions: n >=1
	Postconditions: The number of possble way one may go up or down the stairs.

	"""
	if n == 1:
		return n
	elif n==0:
		return 1
	else:
		#consider an instance, say 4
		#consider s smaller instance, say 3
		#assume count_stairs(3) correctly produces3
		#We can compute the amount of stairs
 		return count_stairs(n-1)+count_stairs(n-2)

m=input('Please enter a number of stairs:')
print count_stairs(m)