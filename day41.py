def build_dict(L1:Iterable[L], L2:Iterable[L]) -> List[L]:
    D = {}
    for i in range(0, min(len(L1), len(L2))):
        D[L1[i]] = L2[i]
    return D
    
print(build_dict([1, 2], ["A", "B"]))
print(build_dict(["A", "B", "C"], ["a", "b", "c"]))
print(build_dict(range(5), [ range(i) for i in range(5) ]))

print(build([1,2],[5.4,3.2]))
