def convert(n):
    if n < 10:
        return placement(n,1)
    if n < 20:
        return teen(n)
    if n >= 20 and n < 100:
        if int(str(n)[1]) == 0:
            return placement(str(n)[0],2)
        else:
            return placement(str(n)[0],2)+convert(int(str(n)[1]))
    elif len(str(n)) == 3:
        x = ""
        h = placement(str(n)[0], 3)
        if int(str(n)[1]) == 0 and int(str(n)[2]) == 0:
            return h
        else:
            z = int(str(n)[1]+"0")+int(str(n)[2])
            x = h + "and" + str(convert(z))
            return x
    elif len(str(n)) == 4:
        x = ""
        t = placement(str(n)[0], 4)
        if int(str(n)[1]) == 0 and int(str(n)[2]) == 0 and int(str(n)[3]) == 0:
            return t
def teen(n):
    n = int(n)
    if n//10 == 1:
        return ["ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen"][n-10]
    else:
        return ""

def placement(n,p):
    n = int(n)
    if n < 1:
        return ""
    if p == 1:
        return ["one","two","three","four","five","six","seven","eight","nine"][n-1]
    if p == 2:
        return ["ten","twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety"][n-1]
    if p == 3:
        return placement(n,1) + "hundred"
    if p == 4:
        return placement(n,1) + "thousand"
b = len(convert(342))
print 'The number of letters for 342 is: ', b, 'letters'
print 'The number of letters for 115 is: ', len(convert(115)), 'letters'
z = 0
for x in range(1,1001):
    z += len(convert(x))
print 'The number of letters used to write out all number between 1 and 1000: ', z, 'letters'




a = []
for x in range(10000):
	x+=1
	if 10000%x==0:
		a.append(x)
if 10000 in a:
	a.remove(10000)
print 'The amicable numbers under 10,000 are: ', a

sum=0
for x in a:
	sum+=x
print 'The sum of all the amicable numbers under 10,000 is: ', sum