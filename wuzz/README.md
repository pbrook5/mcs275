This is a repository for [MCS 275](http://www.math.uic.edu/~lenz/s15.m275) which contains the maze
generation and GUI code.  You should inspect the history of the commits as well as the final
solution, since the commits explain the iterative design of the code.
