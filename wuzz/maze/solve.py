class MazeCell:
    """
    While solving the maze, we store an object of this class for each
    cell we have visited but not yet completly explored.  Thus the main
    property it stores the list of open passages that have not yet been checked.
    """
    def __init__(self, maze, point):
        self.point = point
        self.directions = maze.open_directions(point[0], point[1])

    def pop_direction(self):
        """
        Remove and return a direction that has not yet been checked.
        Returns None if all open passages have been checked.
        """
        if len(self.directions) == 0:
            return None
        else:
            return self.directions.pop()

    def remove_opposite(self, direction):
        """Remove the opposite of the given direction from the list of open directions"""
        opposites = {"N" : "S", "S" : "N", "E" : "W", "W" : "E"}
        self.directions.remove(opposites[direction])

def solve_maze(maze, start=None, end=None):
    """
    Solves a maze finding a path between start and end.  If start and
    end are not given, it finds a path between (0,0) and (maze.rows-1,maze.cols-1)
    """

    if start == None:
        start = (0,0)
    if end == None:
        end = (maze.rows-1, maze.cols-1)

    # All partially explored cells are stored on a stack.  Each entry on the stack
    # is an instance of the MazeCell class.
    stack = []
    stack.append(MazeCell(maze, start))

    while len(stack) > 0:
        # Look at the top of the stack, which is the last element in the list
        c = stack[-1]

        # Get the next passage to look at
        direction = c.pop_direction()

        if direction == None:
            # We have looked at all the open passages for this cell, it can be removed
            # from the stack.  This causes is to go one step "backwards" in our search,
            # backtracking to the previous cell.
            stack.pop()

        else:
            p = maze.cell_in_direction(c.point[0], c.point[1], direction)

            # If this is the end, we have found the path
            if p == end:
                # The path consists of all the cells in the stack
                solution = []
                for c in stack:
                    solution.append(c.point)
                # Plus the end cell
                solution.append(p)
                return solution

            # Otherwise, move to the cell in the given direction
            n = MazeCell(maze, p)
            # Remove the direction we just came from from the list of open passages
            n.remove_opposite(direction)
            stack.append(n)

    # If we reach this, we have explored every possible path from the start since
    # start was popped off the stack, so the maze has no solution.
    return None
