import random
from storage import *
from Tkinter import * # in python 3, instead import tkinter (must be lowercase)

class NewMazeDialog:
    """
    A dialog which prompts the user for a number of rows and columns, and then randomly creates a maze.
    """
    def __init__(self, root):

        self.root = root
        self.maze = None # Will store the maze we eventually create

        # A toplevel is what TKinter calls a new window on the screen.
        self.dialog = Toplevel(root, takefocus=True)
        self.dialog.grab_set()
        self.dialog.transient(root)

        # Now add some labels and spinboxes.  Note that the dialog is given as the parent of
        # each widget, marking that the wdigets should appear in the new window, not the original one.

        self.rowlabel = Label(self.dialog, text="Rows:")
        self.rowlabel.grid(row=0,column=0)

        self.rowbox = Spinbox(self.dialog, from_=10, to=100)
        self.rowbox.grid(row=0, column=1)

        self.collabel = Label(self.dialog, text="Cols:")
        self.collabel.grid(row=1, column=0)

        self.colbox = Spinbox(self.dialog, from_=10, to=100)
        self.colbox.grid(row=1,column=1)

        # Add two buttons and register some handlers
        Button(self.dialog, text="Generate", command=self.generate).grid(row=2, column=0)
        Button(self.dialog, text="Cancel", command=self.cancel).grid(row=2, column=1)
    """def remove_deadends(maze):
        for r in int(maze.rows.get()):
            for c in int(maze.cols.get()):
                d = []
                d = maze.open_directions(r,c)
            	if len(d)>1:
            	    maze.break_wall(r,c,random.choice(d))"""
            	    
            	    
    def generate(self):
        """
        Handler for the Generate button which creates a new maze and closes the dialog
        """
        # Look up the current values from the spinboxes and create the maze
        rows = int(self.rowbox.get())
        cols = int(self.colbox.get())
        self.maze = Maze(rows, cols)
        self.carve_passages_from(self.maze, 0, 0)

        # Remove the dialog from the screen
        self.dialog.destroy()

    def carve_passages_from(self, maze, row, col):
        """
        Recursive function to generate a maze by carving passages starting from the given row
        and column
        """

        # Visit each side of the cell at (row,col) in a random order
        directions = ["N", "S", "E", "W"]
        random.shuffle(directions)
        
        for d in directions:
            p = maze.cell_in_direction(row, col, d)

            # Check if the cell in the given direction is not outside the maze
            # and is not yet visited (not yet visited means the open directions
            # is the empty list).
            if p != None and maze.open_directions(p[0], p[1]) == []:
                maze.break_wall(row, col, d)
                self.carve_passages_from(maze, p[0], p[1])

    def cancel(self):
        """
        Handler for the Cancel button which closes the dialog
        """
        # Remove the dialog from the screen.
        self.dialog.destroy()

    def show_dialog(self):
        """
        Show the dialog on the screen and wait until the dialog is closed.  The return value
        will either be a new maze or None, depending on what the user does.
        """
        # The wait_window function creates a new local event loop and pauses the main event loop.
        # This local event loop will only process events relating to the window given as a parameter
        # (in this case the dialog).  This will cause the user to only be able to interact with the
        # dialog, and nothing else.  This local event loop will continue until the window is closed.
        self.root.wait_window(self.dialog)

        # Python will not reach this line of code until the local event loop ends, which will not happen
        # until the dialog is closed.  That means that either the generate or cancel handlers will have
        # been called, in particular if the user clicked generate, self.maze will have been filled in.
        return self.maze

if __name__ == '__main__':
    # Some test code
    def remove_deadends(maze):
        for r in range(int(maze.rows)):
            for c in range(int(maze.cols)):
                print c
                D=["N","S","E","W"]
                d = []
                d = maze.open_directions(r,c)
                if r ==0:
                    D.remove("N")
                if r==maze.rows-1:
                    D.remove("S")
                if c==0:
                    D.remove("W")
                if c==maze.cols-1:
                    D.remove("E")
                for x in d:
                    D.remove(x)
                if r==0 and c==0 and len(D)==1:
                    maze.break_wall(r,c,D[0])
                if r==0 and c==maze.cols-1 and len(D)==1:
                    maze.break_wall(r,c,D[0])
                if r==maze.rows-1 and c==0 and len(D)==1:
                    maze.break_wall(r,c,D[0])
                if r==maze.rows-1 and c==maze.cols-1 and len(D)==1:
                    maze.break_wall(r,c,D[0])
                if len(d)==1:
                    maze.break_wall(r,c,D[0])
            	if len(D) >2:
            	    l=random.choice(D)
            	    maze.break_wall(r,c,l)
    root = Tk()
    var = IntVar()
    c = Checkbutton(root, text="Deadends", variable=var,onvalue=1, offvalue=0)
    c.pack()
    def opendialog():
        d = NewMazeDialog(root)
        m = d.show_dialog()
        if m:
            if var.get()==1:
                remove_deadends(m)
                m.print_maze()
            else:
                m.print_maze()
        else:
            print ("Maze is none")
    Button(root, text="Test", command=opendialog).pack()
    root.mainloop()
