import sys
import maze
import functools
from Tkinter import * # in python 3, instead import tkinter (must be lowercase)

class MazeGui:
    """
    Main class for the Maze Gui, controlling the main window.
    """
    def __init__(self, root, maze_size = 800):
        """
        Initialize the maze gui.  maze_size is the number of pixels to draw the maze
        """
        self.root = root
        self.maze = None
        self.maze_size = maze_size
        self.user_path = [(0,0)] # The cells that the user has moved through

        root.title("Maze")

        # Create a new menubar (bar across the top)
        menubar = Menu(root)

        # Add a file menu to the menubar.  Note that we use the menubar as the parent of the file menu.
        filemenu = Menu(menubar, tearoff=0)
        # Add a new entry to the file menu, which when clicked will call new_maze
        filemenu.add_command(label="Generate new maze", command=self.new_maze)
        # Add entry to reset the user path
        filemenu.add_command(label="Reset", command=self.reset_user_path)
        # Add the ability to show the solution
        filemenu.add_command(label="Show solution", command=self.show_solution)
        # Add a new entry to the file menu, which when clicked will call root.quit
        # Note that in this case we don't need to create a method, we can just use the root.quit
        # function as the command.
        filemenu.add_command(label="Exit", command=root.quit)
        # Add the filemenu to the menubar
        menubar.add_cascade(label="File", menu=filemenu)

        # Add the menubar to the root window
        root.config(menu=menubar)

        # Add the canvas on which we will draw the maze
        self.canvas = Canvas(root, width=maze_size + 20, height=maze_size + 20, bg='white')
        self.canvas.pack()

        # Add a handler to the keypress events for the arrow keys
        root.bind("<Left>", functools.partial(self.keypress, "W"))
        root.bind("<Right>", functools.partial(self.keypress, "E"))
        root.bind("<Up>", functools.partial(self.keypress, "N"))
        root.bind("<Down>", functools.partial(self.keypress, "S"))

    def new_maze(self):
        """
        Handler for the new maze menu entry, prompting the user for a new maze
        """

        # Create the dialog.  At this point, nothing is on the screen
        dialog = maze.NewMazeDialog(self.root)

        # Show the dialog on the screen.  This function will not return until the dialog
        # is removed from the screen, and will return the new maze or None
        new_maze = dialog.show_dialog()

        # If the user did not cancel the dialog, draw the new maze.  Note that if a maze is already
        # drawn on the canvas and the user clicks cancel, we want to setting self.maze and
        # skip drawing, so that the existing maze still appears.  This is the reason for using a new_maze
        # variable.
        if new_maze:
            self.maze = new_maze
            self.user_path = [(0,0)]
            self.draw()

    def keypress(self, direction, event):
        """
        The handler for the keypress event
        """
        if not self.maze: return

        # The current cell is the last cell on the path
        curCell = self.user_path[-1]

        # Check if the direction the user pressed has a wall or not
        openDirs = self.maze.open_directions(curCell[0], curCell[1]);
        if direction in openDirs:
            # If the direction is open, we now check if the direction is going backwards
            # along the path or to a new cell
            newCell = self.maze.cell_in_direction(curCell[0], curCell[1], direction)
            if len(self.user_path) >= 2 and self.user_path[-2] == newCell:
                # The user pressed the direction to back out of the path, so remove the
                # last cell from the path
                self.user_path.pop()
            else:
                # Add the new cell to the end of the path
                self.user_path.append(newCell)

        self.draw()

    def reset_user_path(self):
        """
        Handler for the reset menu entry, resetting the user path to the start
        """
        if self.maze:
            self.user_path = [(0,0)]
            self.draw()

    def show_solution(self):
        """
        Handler for the show solution menu entry, drawing the solution on the screen.
        This is not permanent and will be lost once the user moves.
        """
        path = maze.solve_maze(self.maze)
        points = map(self.center_of_cell, path)
        self.canvas.create_line(points, fill="blue")

    def draw(self):
        """
        Draw the maze
        """
        self.canvas.delete(ALL)

        # Recall that the coordinates of the canvas have (0,0) in the upper-left corner
        # with positive-x going to the right and positive-y going down.

        # Upper-Left corner of the maze is at (10,10) so there is a gap between the edge
        # of the maze and the edge of canvas.
        startx = 10
        starty = 10

        # Size of each cell
        xcellsize = self.maze_size / self.maze.cols
        ycellsize = self.maze_size / self.maze.rows

        # Draw the border of the maze
        self.canvas.create_rectangle(startx, starty, \
                startx + self.maze.cols * xcellsize, \
                starty + self.maze.rows * ycellsize)

        # For each cell, draw the north and west walls
        for r in range(self.maze.rows):
            for c in range(self.maze.cols):
                
                # Compute coordinate of the upper-left corner of the cell
                x = startx + c * xcellsize
                y = starty + r * ycellsize
        
                open_dirs = self.maze.open_directions(r, c)

                if "N" not in open_dirs:
                    self.canvas.create_line( (x,y), (x+xcellsize, y))
                if "W" not in open_dirs:
                    self.canvas.create_line( (x,y), (x, y + ycellsize))

        # Draw the path the user has moved through
        lastPoint = self.center_of_cell(self.user_path[0])
        for nextCell in self.user_path[1:]:
            nextPoint = self.center_of_cell(nextCell)
            self.canvas.create_line(lastPoint, nextPoint, fill="red")
            lastPoint = nextPoint
        # Draw a circle at the final point on the path
        self.canvas.create_oval(lastPoint[0]-2,lastPoint[1]-2,lastPoint[0]+2,lastPoint[1]+2, fill="red")

    def center_of_cell(self, cell):
        """Returns the canvas coordinates of the center of a cell"""
        row = cell[0]
        col = cell[1]
        xcellsize = self.maze_size / self.maze.cols
        ycellsize = self.maze_size / self.maze.rows
        return (10 + xcellsize * col + xcellsize/2, 10 + ycellsize * row + ycellsize / 2)

sys.setrecursionlimit(10000)
root = Tk()
MazeGui(root)
root.mainloop()
