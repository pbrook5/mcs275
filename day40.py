from typing import Dict

def foo(d: Dict[int,float], i: int) -> [int]:
    ret = [] # type: List[float]
    for num in range(0, i):
        ret.append(d[num])
    return ret


print(foo({1: "One", 2: "Two", 3: "Three"}))