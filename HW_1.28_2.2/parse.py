class Node:
    def __init__(self, val=None, parent=None):
        self.value = val
        self.parent = parent
        self.left = None
        self.right = None

    def new_child(self):
        """
        Adds a new child to either the left or right position
        """
        if self.left == None:
            self.left = Node(parent=self)
            return self.left
        else:
            self.right = Node(parent=self)
            return self.right

    def __str__(self, depth=0):
        """
        Prints the tree rotated to the right
        """
        ret = ""
        if self.right != None:
            ret += self.right.__str__(depth+1)
        ret += "\n" + ("    "*depth) + str(self.value)
        if self.left != None:
            ret += self.left.__str__(depth + 1)
        return ret



def parse_expression(tokens):
    """
    Parses a mathematical expression from a sequence of tokens into a tree.
    This only works if the expression is correctly parentheized
    and the only values are integers.
    """

    root = Node()
    current_node = root
    if node.left == None and node.right == None:
        return node.value
    elif node.left == None and node.right != None:
        all.append(node.right.value)
        tree_max(node.right)
        return max(all)
    elif node.left != None and node.right == None:
        all.append(node.left.value)
        tree_max(node.left)
        return max(all)
    else:
        all.append(tree_max(node.left))
        all.append(tree_max(node.right))
        return max(all)
        
        
    for token in tokens:
        if token == '(':
            # Add a new node
            current_node = current_node.new_child()

        elif token == ')':
            # Move to the parent
            current_node = current_node.parent

        elif token in ["+", "-", "*", "/"]:
            # Set value, add a new child, and make the new child the current node
            current_node.value = token
            current_node = current_node.new_child()

        else:
            # Set value and move to the parent
            current_node.value = int(token)
            current_node = current_node.parent

    return root


def eval_expr_tree (tree):
    """
    Takes a tree and produces a correct mathematical solution using recursion.
    """
    root = Node()
    current_node = root
    solution = 0
    
    for token in tokens:
        if token == '(':
            current_node = current_node.new_child()
        elif token == ')':
            return solution
        elif token == '+':
            return int(tokens[token-1]) + int(tokens[token+1])
        elif token == '-':
            return int(tokens[token-1]) - int(tokens[token+1])
        elif token == '*':
            return int(tokens[token-1]) * int(tokens[token+1])
        elif token == '/':
            return int(tokens[token-1]) / int(tokens[token+1])
        
        
        

tokens = "( ( 4 + 2 ) * ( 3 - 10 ) )".split()

tree = parse_expression(tokens)
# Note the printout is like the tree is rotated (so left is down and right is up)
print tree

print ""
print ""
print "--------------"
print ""
print ""

tokens2 = "( ( ( 12 + 66 ) + ( 3 - ( 6 * 42 ) ) ) * ( 9 + 2 ) )".split()
tree = parse_expression(tokens2)
print tree