class Node:
    def __init__(self, k, val):
        self.key = k
        self.value = val
        self.left = None
        self.right = None
        
def lookup(node, key):
    if node == None:
        return None
    elif node.key == key:
        return node.value
    elif node.key < key:
        return lookup(node.right, key)
    else: # node.key > key
        return lookup(node.left, key)
        
def insert(node, key, val):
    if node.key == key:
        node.value = val

    elif node.key < key and node.right == None:
        # There is no right child so add a new node
        node.right = Node(key, val)
    elif node.key < key: # and node.right != None
        # There is a right child, so insert the new element into that subtree
        insert(node.right, key, val)

    # We now do the same with right replaced with left
    elif node.key > key and node.left == None:
        node.left = Node(key, val)
    elif node.key > key: # and node.left != None
        insert(node.left, key, val)
        
def tree_max(node):
    all=[]
    if node.left == None and node.right == None:
        return node.key
    elif node.left == None and node.right != None:
        all.append(node.right.key)
        tree_max(node.right)
        return max(all)
    elif node.left != None and node.right == None:
        all.append(node.left.key)
        tree_max(node.left)
        return max(all)
    else:
        all.append(tree_max(node.left))
        all.append(tree_max(node.right))
        return max(all)
        
def tree_min(node):
    all=[]
    if node.left == None and node.right == None:
        return node.key
    elif node.left == None and node.right != None:
        all.append(node.right.key)
        tree_min(node.key)
        return min(all)
    elif node.left != None and node.right == None:
        all.append(node.left.key)
        tree_min(node.left)
        return min(all)
    else:
        all.append(tree_min(node.left))
        all.append(tree_min(node.right))
        return min(all)
   
def verify(node):
    if node.left == None and node.right == None:
        return True
    if node.left == None and node.right != None:
        if verify(node.right)==True:
            if tree_min(node.right)>node.key:
                return True
    if node.left != None and node.right == None:
        if verify(node.left)==True:
            if tree_max(node.left)<node.key:
                return True
    if node.left != None and node.right != None:
        if verify(node.left) == True and verify(node.right) == True:
            if verify(node.left) and verify(node.right):
                if tree_max(node.left)<node.key and tree_min(node.right)>node.key:
                    return True
        
    else:
        return False
   
   



root = Node(10, "Hello")
root.left = Node(15, "Fifteen")
print verify(root)

root1 = Node(10, "Ten")
root1.right = Node(20, "Twenty")
root1.left = Node(5, "Five")
root1.left.right = Node(15, "Fifteen")
print verify(root1)