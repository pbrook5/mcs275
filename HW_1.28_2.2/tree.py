class Node:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None
        
def tree_max(node):
    all=[]
    if node.left == None and node.right == None:
        return node.value
    elif node.left == None and node.right != None:
        all.append(node.right.value)
        tree_max(node.right)
        return max(all)
    elif node.left != None and node.right == None:
        all.append(node.left.value)
        tree_max(node.left)
        return max(all)
    else:
        all.append(tree_max(node.left))
        all.append(tree_max(node.right))
        return max(all)
        
        
root = Node(5)
root.left = Node(15)
root.right = Node(22)
root.left.left = Node(4)
root.left.right = Node(2)
root.right.right = Node(33)



print tree_max(root)