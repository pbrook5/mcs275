from Tkinter import *
import tkMessageBox
import sqlite3
import os

class Program:
    def __init__(self, root):
        self.create_db()
        self.conn = sqlite3.connect("library.sqlite")
        # Enable foreign keys
        self.conn.execute("PRAGMA foreign_keys = ON")
        self.build_gui(root)

    def create_db(self):
        if not os.path.isfile("library.sqlite"):
            conn = sqlite3.connect("library.sqlite")
            conn.execute("CREATE TABLE books (book_id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, author TEXT, isbn TEXT)")
            conn.execute("CREATE TABLE people (person_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, email TEXT)")
            conn.execute("""CREATE TABLE loans (book_id INTEGER PRIMARY KEY,
                                                person_id INTEGER,
                                                FOREIGN KEY(book_id) REFERENCES books(book_id),
                                                FOREIGN KEY(person_id) REFERENCES people(person_id)
                                                )""")
            conn.commit()
            conn.close()

    def build_gui(self, root):

        # Book
        lbl = Label(root, text="Book title")
        lbl.grid(row=0, column=0)
        self.book_title = Entry(root, width=10)
        self.book_title.grid(row=0, column=1)
        lbl = Label(root, text="author")
        lbl.grid(row=0, column=2)
        self.book_author = Entry(root, width=10)
        self.book_author.grid(row=0, column=3)
        lbl = Label(root, text="isbn")
        lbl.grid(row=0, column=4)
        self.book_isbn = Entry(root, width=10)
        self.book_isbn.grid(row=0, column=5)
        add = Button(root, text="Add book", command=self.add_book)
        add.grid(row=0, column=6)

        # Search books
        lbl = Label(root, text="Search for author: ")
        lbl.grid(row=1, column=0)
        self.author_search = Entry(root, width=10)
        self.author_search.grid(row=1, column=1)
        search = Button(root, text="Search", command=self.search_books)
        search.grid(row=1, column=2)
        all = Button(root, text="All Books", command=self.all_books)
        all.grid(row=1, column=6)

        # People
        lbl = Label(root, text="Person name")
        lbl.grid(row=2, column=0)
        self.person_name = Entry(root, width=10)
        self.person_name.grid(row=2, column=1)
        lbl = Label(root, text="email")
        lbl.grid(row=2, column=2)
        self.person_email = Entry(root, width=10)
        self.person_email.grid(row=2, column=3)
        add = Button(root, text="Add person", command=self.add_person)
        add.grid(row=2, column=4)

        # Search People
        lbl = Label(root, text="Search people: ")
        lbl.grid(row=3, column=0)
        self.search_person = Entry(root, width=10)
        self.search_person.grid(row=3, column=1)
        search = Button(root, text="Search", command=self.search_people)
        search.grid(row=3, column=2)
        all = Button(root, text="All People", command=self.all_people)
        all.grid(row=3, column=6)

        # Lend
        lbl = Label(root, text="Book ID")
        lbl.grid(row=4, column=0)
        self.book_id = Entry(root, width=10)
        self.book_id.grid(row=4, column=1)
        lbl = Label(root, text="Person ID")
        lbl.grid(row=4, column=2)
        self.person_id = Entry(root, width=10)
        self.person_id.grid(row=4, column=3)
        lend = Button(root, text="Lend book", command=self.lend_book)
        lend.grid(row=4, column=4)
        show_lent = Button(root, text="Show lent books", command=self.show_lent)
        show_lent.grid(row=4, column=6)

        # Return
        lbl = Label(root, text="Return Book ID")
        lbl.grid(row=5, column=0)
        self.return_id = Entry(root, width=10)
        self.return_id.grid(row=5, column=1)
        rtn = Button(root, text="Return Book", command=self.return_book)
        rtn.grid(row=5, column=2)

        # Listbox for results
        self.content = Listbox(root, width=60)
        self.content.grid(row=6, column=0, columnspan=7)

    def display_error(self, err):
        """Display an error in a message box"""
        tkMessageBox.showerror("Error", err)

    def add_book(self):
        if self.book_title.get() == "":
            self.display_error("name is empty")
            return
        if self.book_author.get() == "":
            self.display_error("author is empty")
            return
        if self.book_isbn.get() == "":
            self.display_error("isbn is empty")
            return

        # Start a new transaction and automatically commit or rollback at the end of the with block
        with self.conn:
            # Insert the row, using the data values from the corresponding controls on the screen.
            # Note how we list off the column names which correspond to the three ?, and we have left off
            # book_id.  This is because we marked the book_id column as AUTOINCREMENT, so sqlite will 
            # automatically assign an id during this insert.
            self.conn.execute("INSERT INTO books(title, author, isbn) VALUES (?,?,?)",
                                [self.book_title.get(), self.book_author.get(), self.book_isbn.get()])

    def search_books(self):
        # Clear whatever is in the listbox
        self.content.delete(0, END)

        # search by author, displaying the results in the listbox
        for row in self.conn.execute("SELECT * FROM books WHERE author = ?", [self.author_search.get()]):
            self.content.insert(END, row)

    def all_books(self):
        # Clear whatever is in the listbox
        self.content.delete(0, END)

        # load all books (note the missing WHERE clause)
        for row in self.conn.execute("SELECT * from books"):
            self.content.insert(END, row)

    def add_person(self):
        if self.person_name.get() == "":
            self.display_error("name is empty")
            return
        if self.person_email.get() == "":
            self.display_error("email is empty")
            return

        # Start a new transaction and automatically commit or rollback at the end of the with block
        with self.conn:
            # Insert the row, using the data values from the corresponding controls on the screen.
            # Again, the person_id will be automatically assigned by sqlite.
            self.conn.execute("INSERT INTO people(name, email) VALUES (?,?)",
                                [self.person_name.get(), self.person_email.get()])

    def search_people(self):
        # Clear whatever is in the listbox
        self.content.delete(0, END)

        # search by author, displaying the results in the listbox
        for row in self.conn.execute("SELECT * FROM people WHERE name = ?", [self.search_person.get()]):
            self.content.insert(END, row)

    def all_people(self):
        # Clear whatever is in the listbox
        self.content.delete(0, END)

        for row in self.conn.execute("SELECT * FROM people"):
            self.content.insert(END, row)

    def lend_book(self):

        try:

            # Start a new transaction
            with self.conn:
                # Attempt to insert lending a book.  If either the book id or the person id
                # is missing, the foreign key constraint will be violated and the database will
                # raise an error.  The with statement will then abort the transaction.
                self.conn.execute("INSERT INTO loans (book_id, person_id) VALUES (?,?)",
                                       [ int(self.book_id.get()), int(self.person_id.get())])
        except Exception as e:
            self.display_error(str(e))

    def show_lent(self):
        # Clear whatever is in the listbox
        self.content.delete(0, END)

        # join all three tables together to obtain the lent books and their info
        cmd = """SELECT * FROM loans
                            INNER JOIN books ON loans.book_id = books.book_id
                            INNER JOIN people ON loans.person_id = people.person_id"""

        for row in self.conn.execute(cmd):
            self.content.insert(END, row)
        

    def return_book(self):
        try:
            with self.conn:
                self.conn.execute("DELETE FROM loans (book_id, person_id) VALUES (?,?)",
                                       [ int(self.book_id.get()), int(self.person_id.get())])
        except Exception as e:
            self.display_error(str(e))
        return

root = Tk()
application = Program(root)
root.mainloop()