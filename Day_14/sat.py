import math
import jug
from datetime import *
from sgp4.earth_gravity import wgs72
from sgp4.io import twoline2rv

def dist(p,q):
    v = 0
    for i in range(3):
        v += (p[i] - q[i])**2
    return math.sqrt(v)

def compute_sat_positions(start_date, line1, line2):
    """
    Computes satellite positions for the next 5 days in 1 minute increments.
    """
    positions = []
    d = start_date
    sat = twoline2rv(line1, line2, wgs72)

    for i in range(5*24*60): # 5 days with 24 hours each and 60 minutes per hour

        # compute position and velocity of the satellite
        pos, vol = sat.propagate(d.year, d.month, d.day, d.hour, d.minute, d.second)
        positions.append(pos)

        # increment by one minute
        d = d + timedelta(minutes=1)

    return positions

def check_close_approach(sat_positions, start_date, debris_name, line1, line2):
    """
    Simlutate the debris position for 5 days in 1 minute increments and tests if
    it comes with 15 kilometers of the sattelite position.

    * sat_positions is a list of the satellite positions starting at start_date with one
      position every 10 minutes
    * debris_name is a string for the debris name
    * line1 and line2 are the NORAD 2-line current data for the debris

    This function returns either None if the debris does not come with 15 kilometers
    of the satellite or returns (debris_name, date) for the close approach.
    """

    d = start_date
    debris = twoline2rv(line1, line2, wgs72)

    for i in range(5*24*60): # 5 days with 24 hours per day and 60 minutes per hour

        # Compute location and velocity of the debris
        pos, vol = debris.propagate(d.year, d.month, d.day, d.hour, d.minute, d.second)

        # Check distance between pos with the iss_position
        if pos != None:
            if dist(pos, sat_positions[i]) <= 15:
                return (debris_name, d)

        # Increment the time by one minute
        d = d + timedelta(minutes=1) # add one minute to the 

    # No close apporach found
    return None


#------------------------------------------------------

start = datetime(2015, 2, 12, 0, 0, 0)

iss_task = jug.Task(compute_sat_positions, start,
        "1 25544U 98067A   15043.89889757  .00024357  00000-0  36611-3 0  9995",
        "2 25544  51.6481 344.7340 0005869   1.8000 104.1365 15.54705052928697")

debris_tasks = []

f = open("cosmos-2251-debris.txt", "r")
while True:
    name = f.readline()
    line1 = f.readline()
    line2 = f.readline()
    if not line2: break
    debris_tasks.append(jug.Task(check_close_approach, iss_task, start, name, line1, line2))