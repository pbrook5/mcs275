import functools
from Tkinter import *

class Calculator:
    def __init__(self, root):
        root.title("Simple Calculator")

        # use Entry widget for an editable display
        self.entry = Entry(root, width=33, bg="yellow")
        self.entry.grid(row=0, column=0, columnspan=5)
        self.entry.bind("<Double-Button-1>", self.double_click)

        # The button text to display
        btn_list = [
            ['7',  '8',  '9',  '*',  'C'],
            ['4',  '5',  '6',  '/',  'M->'],
            ['1',  '2',  '3',  '-',  '->M'],
            ['0',  '.',  '=',  '+',  'neg']
        ]

        # Create all the buttons
        for row in range(4):
            for col in range(5):
                key = btn_list[row][col]
                # Create a button
                b = Button(root, text=key, width=5)
                b.grid(row = row + 1, column = col)

                # Bind the btn_click method to the button.  Since tkinter
                # expects the handler to take no parameters but we want
                # to pass to the handler which key was pressed, we use
                # http://docs.python.org/2/library/functools.html#functools.partial
                # to partially apply the function (fix the key argument).
                #
                # The button event/command is another example of a virtual
                # event, since this event will be fired on mouse clicks but
                # also on <Spacebar> after tabbing to the control.
                b.config(command = functools.partial(self.btn_click, key))

        self.memory = ""


    def btn_click(self, key):
        if key == '=':
            # avoid division by integer
            if '/' in self.entry.get() and '.' not in self.entry.get():
                self.entry.insert(END, ".0")

            # here comes the calculation part
            try:
                result = eval(self.entry.get())
                self.entry.insert(END, " = " + str(result))
            except:
                self.entry.insert(END, "--> Error!")

        elif key == 'C': # Clear
            self.entry.delete(0, END)

        elif key == 'neg':
            # If the entry has a result, clear it first
            if '=' in self.entry.get():
                self.entry.delete(0, END)

            # Now try and insert a minus, checking for existing minus first
            try:
                if self.entry.get()[0] == '-':
                    self.entry.delete(0)
                else:
                    self.entry.insert(0, '-')
            except IndexError:
                pass

        elif key == '->M':
            # Store the current value in the memory variable
            self.memory = self.entry.get()

            # If the current text has an equal sign, we only want to
            # store the text after the equal sign
            if '=' in self.memory:
                ix = self.memory.find('=')
                self.memory = self.memory[ix+2:]

        elif key == 'M->':
            # Add the contents of memory to the end
            self.entry.insert(END, self.memory)

        else:
            # If the entry has a result, clear it first
            if '=' in self.entry.get():
                self.entry.delete(0, END)

            # Add the key that was clicked to the entry
            self.entry.insert(END, key)
            
    def double_click(self,key):
        self.entry.delete(0, END)

root = Tk()
Calculator(root)
root.mainloop()