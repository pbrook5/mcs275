# MCS 275 Spring 2015

from connectfour import *
from player import *
from minimax import *
from utility import *
from Tkinter import *
import tkMessageBox 

class MazeGui:
	def __init__(self, root, board, cell_size = 100):
		root.title("Connect Four")
		self.board = board
		self.cell_size = cell_size
		self.rows = 6
		self.cols = 7
		self.maze_height = self.rows * self.cell_size
		self.maze_width = self.cols * self.cell_size
		self.turnCount =1
		self.p1 =Human(playernum=1)
		self.p2 =Human(playernum=2)
     

		rlabel = Label(root, text ="Ply Depth:")
		rlabel.grid(row=0,column=0)
        
		self.entry_box = Entry(root, width =5)
		self.entry_box.grid(row=0, column=1)
        

        
        #player 1 Labeling
		rlabel = Label(root, text ="Player1:")
		rlabel.grid(row=0,column=2)

		self.player1_type = StringVar(root)
		options = ["Human", "Random", "MiniMax"]
		self.player1_type.set(options[0])
		self.rowbox = OptionMenu(root, self.player1_type, *options)
		self.rowbox.grid(row=0, column=3)

        #player 2 Labeling
		rlabel = Label(root, text ="Player2:")
		rlabel.grid(row=0,column=4)

		self.player2_type = StringVar(root)
		options = ["Human", "Random", "MiniMax"]
		self.player2_type.set(options[2])
		self.rowbox = OptionMenu(root, self.player2_type, *options)
		self.rowbox.grid(row=0, column=5)
        
    

        #start button labeling
		self.startbtn = Button(root, text="Start", command = self.game_start)
		self.startbtn.grid(row=0, column=6)
		
		self.resetbrn = Button(root,text='Reset',command=self.restart)
		self.resetbrn.grid(row=0,column=7)
		

		play_col=[]
		for i in range(0,self.cols):
			play_col.append(Button(root, text="col %d" %i, command = lambda col=i:self.btn_click(col)))
			play_col[i].grid(row=1, column=i)
                            
		self.canvas = Canvas(root, width = self.maze_width +10, height = self.maze_height +10, bg="grey")
		self.canvas.grid(row = 2, column = 0, columnspan = self.cols)
		self.canvas.create_line(0,0,10,10,fill='red', width=3);


		self.draw()
		self.grid()

	def game_start(self):
		if self.player1_type.get() !="Human" and self.player2_type.get() !="Human" : #error message
			print "There must be one human."
			tkMessageBox.showinfo(title="Attention", message= "There must be one human.")
			return

		entry = self.entry_box.get()
		if entry.isdigit():
			depth=int(entry)
			if depth  <= 0 or depth > 4:
				print "Ply depth should be 1-4!"
				tkMessageBox.showinfo(title="Attention", message= "Ply depth must be 1-4!")
				return
		else:
			print "Ply depth should be between 1-4!"
			tkMessageBox.showinfo(title="Attention", message= "Ply depth must be 1-4!")
			return
        
		if self.player1_type.get() =="Human" :
			self.p1 = Human(playernum=1)
		elif self.player1_type.get() =="MiniMax" :
			self.p1 = MinimaxPlayer(playernum=1, ply_depth=depth, utility=SimpleUtility(1, 5))
		elif self.player1_type.get() =="Random":
			self.p1 = RandomPlayer(playernum =1)

		if self.player1_type.get =="MiniMax" or self.player2_type.get() =="Random": 
			self.p1.play_turn(self.board)
			self.board.print_board()
			self.draw_board()
			self.turnCount = self.turnCount +1

            
            
		if self.player2_type.get() =="Human" :
			self.p2 = Human(playernum=2)
		elif self.player2_type.get() =="MiniMax" :
			self.p2 = MinimaxPlayer(playernum=2, ply_depth=depth, utility=SimpleUtility(1, 5))
		elif self.player2_type.get() =="Random":
			self.p2 = RandomPlayer(playernum =2)

		if self.player2_type.get =="MiniMax" or self.player1_type.get() =="Random":
			self.p2.play_turn(self.board)
			self.board.print_board()
			self.draw_board()
			self.turnCount = self.turnCount +1

            
		if self.player1_type == "MiniMax":
			self.p1 = MinimaxPlayer(playernum=1, ply_depth=depth, utility=SimpleUtility(1, 5))
		elif self.player1_type == "Random":
			self.p1 = RandomPlayer(playernum =1)

		elif self.player2_type == "MiniMax":
			self.p2 = MinimaxPlayer(playernum=2, ply_depth=depth, utility=SimpleUtility(1, 5))
			
		elif self.player2_type == "Random":
			self.p2 = RandomPlayer(playernum =2)
        

	def btn_click(self, col):
		playerNum = self.turnCount %2
		if playerNum == 0:
			playerNum =2

		self.board.play_turn(playerNum,col)
		self.board.print_board()
		self.draw_board()

        
		w = self.board.is_game_over()
		if w != None:
			print "Congradulations Player %i, you won!" % w
			tkMessageBox.showinfo(title="Congradulations", message= "Player %i, you won!" % w)
			return

		self.turnCount = self.turnCount +1
		nextPlayerNum=self.turnCount%2
		if nextPlayerNum == 0:
			nextPlayerNum =2


		if self.player1_type.get()  != "Human" and nextPlayerNum==1:
			self.p1.play_turn(self.board)
			self.board.print_board()
			self.draw_board()
		elif self.player2_type.get() != "Human" and nextPlayerNum==2: 
			self.p2.play_turn(self.board)
			self.board.print_board()
			self.draw_board()
            
		w = self.board.is_game_over()
		if w != None:
			print "Congradulations Player %i, you won!" % w
			tkMessageBox.showinfo(title="Congradulations", message= "Player %i, you won!" % w)
			return

		self.turnCount = self.turnCount +1

	def grid(self):
		dif = self.maze_width/self.cols
        
		self.canvas.create_rectangle(10,10,10+self.cols * self.cell_size,10+self.rows * self.cell_size)
		for i in range(0,6):
			self.canvas.create_line(dif+(i*dif)+10,10, dif+(i*dif)+10, self.maze_height+10)
		for i in range(0,7):
			self.canvas.create_line(10,dif+(i*dif)+10,self.maze_width+10, dif+(i*dif)+10, fill="gray")

	def restart(self):
		self.canvas.delete(ALL)
		self.grid()
		self.board = [[None for col in range(7)] for row in range(6)]
		self.game_start

	def draw_circle(self, row, col, player):
		pos_row = 10+row * self.cell_size
		pos_col = 10+col * self.cell_size
		if player == 1:
			self.canvas.create_oval(pos_col,pos_row, pos_col+self.cell_size, pos_row + self.cell_size, fill = "red")
		else:
			self.canvas.create_oval(pos_col,pos_row, pos_col+self.cell_size, pos_row + self.cell_size, fill = "green")
	def draw_win_line(self,L):
		self.canvas.create_line(L[0]+L[2]+10*self.cell_size, L[1]+L[3]+10*self.cell_size, L[0]+2*L[2]+10*self.cell_size, L[1]+2*L[3]+10*self.cell_size,fill = 'white')	

	def draw(self):
		self.canvas.delete(ALL)
		starx = 10
		stary = 10

		# Draw the border of the maze
		self.canvas.create_rectangle(starx, stary, \
		starx + self.cols * self.cell_size, \
		stary + self.rows * self.cell_size)
		self.draw_board()

	def draw_board(self):
		for row in range(5,-1,-1):
			for col in range(7):
				p = self.board.get_position(row, col)
				if p == 1 or p == 2:
					pos = 5-row 
					self.draw_circle(pos, col, p)
               
	def play_game(self,board, player1, player2):

		global draw_win_line
		player1.play_turn(board)
		draw_board(board)
		w = self.board.is_game_over()
		if w != None:
			print "Congradulations Player %i, you won!" % w
			tkMessageBox.showinfo(title="Congradulations", message= "Player %i, you won!" % w)
			return

		player2.play_turn(board)
		draw_board(board)
		w = self.board.is_game_over()
		if w != None:
			print "Congradulations Player %i, you won!" % w
			tkMessageBox.showinfo(title="Congradulations", message= "Player %i, you won!" % w)
			return

board = ConnectFour()
root = Tk()
mgui = MazeGui(root,board)
root.mainloop()
root.after(1000, mgui.play_game(board, p1, p2))

