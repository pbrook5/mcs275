import sqlite3
import os

if os.path.isfile('test.db'):
    os.remove('test.db')

 # This will create a new empty database, since we just deleted the file if it exists.
conn = sqlite3.connect('test.db')

 # Create an L_rides table and specify the columns and their types.
conn.execute("CREATE TABLE L_rides (station_id INTEGER, stationname TEXT, date TEXT, daytype TEXT, rides INTEGER)")

 # Insert two rows into the L_rides database.  Note how the SQL expression has five question marks:
 # these question marks coorespond to the five values we have given in the list as the second
 # argument to conn.execute.  Also notice the values to insert match the ordering of the columns
 # in the above CREATE TABLE statement.
conn.execute("INSERT INTO L_rides VALUES (?,?,?,?,?)", [40350, "UIC-Halstead", "2015-03-30", "U", 2345])
conn.execute("INSERT INTO L_rides VALUES (?,?,?,?,?)", [40470, "Racine", "2015-03-30", "U", 6789])
conn.execute("INSERT INTO L_rides VALUES (?,?,?,?,?)", [10344, "Jackson", "2015-03-30", "U", 999])

 # Print all the rows in the L_rides table.  The * stands for all columns.  If we didn't
 # want all columns, we could replace the * with a list of columns.
for row in conn.execute("SELECT * FROM L_rides"):
    print row

 # Update the rides of one of the rows.  Notice how only the row with id 40470 is updated,
 # the other row is left alone.
conn.execute("UPDATE L_rides SET rides = ? WHERE station_id = ?", [4444, 40470])

 # Print out all the rows again.
for row in conn.execute("SELECT * FROM L_rides"):
    print row

def rides_bigger_than(x):
    print "Searching for rows with rides larger than %i" % x
    for row in conn.execute("SELECT * FROM L_rides WHERE rides > ?", [x]):
        print row

def rides_smaller_than(x):
    print "Searching for rows with rides smaller than %i" % x
    for row in conn.execute("SELECT * FROM L_rides WHERE rides < ?", [x]):
        print row

rides_bigger_than(1000)
rides_bigger_than(998)
rides_smaller_than(1000)

conn.commit()
conn.close()


