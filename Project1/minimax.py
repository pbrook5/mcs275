from connectfour import *
from player import *
from utility import *

class MinimaxNode(ConnectFour):

    @staticmethod
    def init_root(copyfrom, nodeplayer):
        """
        Creates a new instance of the MinimaxNode class, with the board state copied
        from the copyfrom object, which is an instance of the ConnectFour class.  
        
        Also, create two properties:
        * a nodeplayer property, storing the fact that this node is a play for nodeplayer.
        * a from_parent_column property set to None (see below).  This marks that the
          node did not come from a parent.

        This method should only be used to create the root of the game tree from
        the current board state.
        """
        node = MinimaxNode()
        # TODO: set properties on node
        # Be careful about copying. You must copy the lists you are using for storage.
        node.board = [[copyfrom.get_position(row, col) for col in range(7)] for row in range(6)]
        node.nodeplayer = nodeplayer
        node.from_parent_column = None
        return node

    @staticmethod
    def init_child(parent, playcolumn):
        """
        Creates a MinimaxNode by copying the board state from parent (which is an instance
        of MinimaxNode) and then making a play in playcolumn (which is an integer
        between 0 and 6).

        Also, create two properties:
        * nodeplayer property, which is the opposite of the parent nodeplayer, representing
          that the child is now a play for the other player.
        * from_parent_column property, which stores the playcolumn parameter.  This is used to
          know which column was played to to get from the parent to the child, and is used
          at the very end to know which play to make.
        """
        node = MinimaxNode()
        # TODO: set node property storing the board by copying from parent
        node.board = [[parent.get_position(row, col) for col in range(7)] for row in range(6)]
        node.play_turn(parent.nodeplayer, playcolumn)
        node.nodeplayer = 3 - parent.nodeplayer
        node.from_parent_column = playcolumn
        return node

    def compute_children(self):
        """
        Computes the list of children of this node and stores it in a property called children.
        This method only creates the immediate children (this is lazy evaluation).
        """
        # Iterate the non-full columns, createing a new node for each.
        self.children = []
        for col in range(7):
            if self.get_position(5, col) == None:
                self.children.append(MinimaxNode.init_child(self, col))

    def set_minimax_value(self, val):
        """
        Stores the minimax value for this node.  This represents the largest utility
        that can be forced on a leaf no matter how the enemy plays.
        """
        self.value = val

    def get_minimax_value(self):
        """
        From this node, it is possible to reach a leaf with this utility.
        """
        return self.value

class MinimaxPlayer(Player):
    def __init__(self, playernum, ply_depth, utility):
        super(MinimaxPlayer, self).__init__(playernum)
        self.ply_depth = ply_depth
        self.utility = utility

    def minimax(self, node, cur_depth):
        """
        This is the recursive procedure for minimax.  It computes the number N where
        N is the best utlity that can be forced on a leaf.  This value N is stored
        in the tree node by calling set_minimax_value and is also returned from this
        method.
        
        The cur_depth is used to distinguish min/max nodes and the recursion ends
        once cur_depth equals self.ply_depth
        """

        # The code is similar to the code from class, except the values need
        # to be recorded on the node using set_minimax_value and the all the
        # alpha and beta stuff must be removed.

        if cur_depth == self.ply_depth:
            l = self.utility.compute_utility(node, self.playernum)
            node.set_minimax_value(l)
            return l

        node.compute_children()

        # Maximum
        if cur_depth % 2 == 0:
            v = -5000000
            for child in node.children:
                v = max(v, self.minimax(child, cur_depth + 1))
            node.set_minimax_value(v)
            return v

        else:
            v = 5000000
            for child in node.children:
                v = min(v, self.minimax(child, cur_depth + 1))
            node.set_minimax_value(v)
            return v

    def play_turn(self, board):
        # Initialize the board as a root node and run minimax.  Then find the child of the
        # root with the largest value and use the child.from_parent_column property to play
        # a board move.
        root = MinimaxNode.init_root(board, self.playernum)
        childval = self.minimax(root, 0)
        print "Completed with value ", childval
        for child in root.children:
            if child.get_minimax_value() == childval:
                board.play_turn(self.playernum, child.from_parent_column)
                return

        assert False
        
# Investigations

# 1. Minimax won every time with around 6 turns. Once the ply was increased, Minimax
#    continued to win but in a fewer amount of turns (around 4).
# 2. Minimax can be beat but it does try to block you moves.
# 3. When minimax depth is increased to 3, it can still be beat but when the depth is 
#    set to 4 the minimax begins to block you earlier. Before minimax would only block
#    when it found 3 in a row but once the depth was set to 4 it would begin to block 
#    after it found 2 in a row.
# 4. When a minimax depth 2 and a minimax depth 3 are playing each other, order of turn
#    became very important. When depth 3 would go first the game would end up with no
#    winner and a  full board but when depth 2 would start the game, depth 2 would win
#    every time!
# 5. When depths 2 and 4 play each other and depth 4 starts the game, depth 2 often wins.
#    When the tables are turned and depth 2 goes first, depth 2 still won.
# 6. When the point values are changed, who goes first is a big factor. Typically, the
#    second player would win. The smaller the point values the longer the game would go 
#    on for. Conversely, the larger the point values were, the shorter the game was.
# 7. When a depth 4 plays a depth 2, it the board looks like the depth 4 was on the
#    offensive while player 2 was on the defense until an opportunity presented itself.
# 8. When the column utility is utilized the minimax becomes much more difficult to beat.
#    When the column utility is used, the minimax is more on the offensive.
# 9. When a simple utility and column utility play each other, when the simple utility
#    has a deeper depth than the column the column still ends up winning. When the point
#    values are manipulated, the column utility player typically wins. The higher the
#    point values, the longer the games are. Likewise, the smaller the point values, the 
#    game is much shorter.
# 10. When two column utility players play each other with same attributes it comes down
#     to who took the first move. Typically the first player won the game. When the point
#     values were manipulated it seemed that the first player still won but that the
#     values had a direct effect on the length of the game. Many of the higher point value
#     resulted in a shorter, some as quick as ten turns.
# 11. For larger configurations, points should be granted for "trapping" your opponent or
#     setting them in an impossible situation. Deeper plys would detect this so the points
#     granted should be proportional to the ply depth.