import inspect

class Hanoi:
    def __init__(self, n):
        self.peg0 = range(1,n+1)
        self.peg1 = []
        self.peg2 = []
        self.pegs = [self.peg0, self.peg1, self.peg2]

    def move(self, src, dst):
        """Move a single disk from src to dest"""
        speg = self.pegs[src]
        dpeg = self.pegs[dst]
        
        # Check that source is smaller than dest
        assert len(dpeg) == 0 or speg[0] < dpeg[0]
        
        # Remove the 0th element from speg and insert it to dpeg
        x = speg.pop(0)
        dpeg.insert(0, x)

        self.print_frames_and_pegs("Just moved from %s to %s" % (src, dst))

    def spare(self, src, dst):
        """Returns the peg which is not src and dst"""
        return 3 - src - dst

    def print_frames_and_pegs(self, msg):
        print msg
        print "  Peg 0 %s" % self.peg0
        print "  Peg 1 %s" % self.peg1
        print "  Peg 2 %s" % self.peg2
        for f in inspect.getouterframes(inspect.currentframe()):
            if f[3] == "hanoi":
                print "  Frame: %s paused at line %i" % (str(inspect.getargvalues(f[0]).locals), f[2])
        print "" # Blank line