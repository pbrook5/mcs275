with open('words.txt','r') as f:
    words = f.read().split(',')
    words = [list(word.strip('\"')) for word in words]
    f.close()



#y = raw_input('Please enter a word: ')
#str(y)

m = max([len(w) for w in words])
print words
total = 0
total2 = 0
vals = {}
vals2=[]
s = list("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
for c in s:
    vals[c] = s.index(c) + 1
    
triangles = [n*(n + 1)/2 for n in range(1,100*m)]
pent = [n*(3*n-1)/2 for n in range(1,100*m)]
hex = [(n*(2*n-1)) for n in range(1,100*m)]

for y in words:

    if sum([vals[d] for d in y]) in triangles:
        total += 1
        print 'The word ' ,y, ' is a triangle word'
    else:
        print 'The word ', y,' is not a triangle word'
    
    if sum([vals[d] for d in y]) in hex:
        print 'The word ' ,y, ' is a hexagonal word'
    else:
        print 'The word ', y,' is not a hexagonal word'
    
    if sum([vals[d] for d in y]) in pent:
        print 'The word ' ,y, ' is a pentagonal word'
    else:
        print 'The word ', y,' is not a pentagonal word'
    
    if sum([vals[d] for d in y]) in pent and hex and triangles:
        total2 +=1
    
print 'The Total number of triangle words is: ', total
print 'The total number of words that are all three is: ', total2

for n in range(1,10000000):
    if n in pent and hex and triangles:
        vals2.append(n)

next = vals2.index(40755)+1
print 'The next number that is all three after 40755 is : ', vals2[next]

# Database normalization is when a database has multiple tables that are joined together to
# create the origianl table. The multiple tables gives the database the ability to be less
# repetitive and therefore more efficient.