import csv
import datetime
import os
import sqlite3
import sys
import urllib

if os.path.isfile("cta.db"):
    print "File already exists"
    sys.exit(0)

conn = sqlite3.connect("cta.db")

 # Create an L_rides table and specify the columns and their types.
conn.execute("CREATE TABLE L_rides (station_id INTEGER, stationname TEXT, date TEXT, daytype TEXT, rides INTEGER)")

 # Create an L_stops table
conn.execute("CREATE TABLE L_stops (station_id INTEGER, stop_id INTEGER, " + \
                                   "stationname TEXT, stopname TEXT, direction TEXT, " + \
                                   "ada INTEGER, location TEXT, " + \
                                   "red INTEGER, blue INTEGER, green INTEGER, " + \
                                   "brown INTEGER, purple INTEGER, purple_express INTEGER, " + \
                                   "yellow INTEGER, pink INTEGER, orange INTEGER)")

 # Fill in L_rides table from the CSV data downloaded from the website
with open("Lrides.csv", "r") as csvfile:
    data = csv.reader(csvfile)
    # Skip the first row, which is the headers
    data.next()

    # Now iterate the remaining rows
    for row in data:
        # The date is formated as month/day/year but we want it stored as year-month-day.
        d = datetime.datetime.strptime(row[2], "%m/%d/%Y")
        # Now convert it to something like 2014-04-15
        dstr = d.strftime("%Y-%m-%d")

        # Now add the row to the database
        conn.execute("INSERT INTO L_rides VALUES (?,?,?,?,?)", \
               [row[0], row[1], dstr, row[3], row[4]])



 # Fill in the L_stops table from the CSV data downloaded from the website
with open("Lstops.csv", "r") as csvfile:
    data = csv.reader(csvfile)
    data.next() # skip the first row
    
    for row in data:
        # The columns in order are
        # 0 - STOP_ID,
        # 1 - DIRECTION_ID
        # 2 - STOP_NAME
        # 3 - STATION_NAME
        # 4 - STATION_DESCRIPTIVE_NAME
        # 5 - MAP_ID  (This looks to be the station_id in the other table)
        # 6 - ADA
        # 7 - RED
        # 8 - BLUE
        # 9 - G  (this must be green)
        # 10 - BRN
        # 11 - P  (must be purple.  I thought it might be pink but pink is below.)
        # 12 - Pexp  (purple express)
        # 13 - Y (yellow)
        # 14 - Pnk
        # 15 - O (orange)
        # 16 - Location

        def istrue(s):
            "Checks if a string is equal to true"
            return s.lower() == "true"

        conn.execute("INSERT INTO L_stops VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                [ row[5], # map_id/station_id
                  row[0], # stop id
                  row[3], # station name
                  row[2], # stop name
                  row[1], # direction
                  istrue(row[6]), # ada
                  row[16], # location
                  istrue(row[7]), # red
                  istrue(row[8]), # blue
                  istrue(row[9]), # green
                  istrue(row[10]), # brown
                  istrue(row[11]), # purple
                  istrue(row[12]), # purple express
                  istrue(row[13]), # yellow
                  istrue(row[14]), # pink
                  istrue(row[15])]) # orange



conn.commit()
conn.close()