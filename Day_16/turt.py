import turtle
import random

speed=100

class AnimatedTurtle(turtle.Turtle):

    
    def __init__(self, x, y, height, width):
        super(AnimatedTurtle, self).__init__()

        # Initialize the turtle to the x, y coordinates with a random heading
        global speed
        self.up()
        self.tracer(0)
        self.goto(x,y)
        self.tracer(1)
        self.shape("turtle")
        self.width = width
        self.height = height
        self.setheading(random.randint(1, 359))

        # Register a timer event to fire after 100 milliseconds
        turtle.ontimer(self.move_one_step, speed)

    def move_one_step(self):

        # Check for edge of screen
        global speed
        xpos, ypos = self.position()
        if xpos < 0 or xpos > self.width:
            self.setheading(180 - self.heading())
        if ypos < 0 or ypos > self.height:
            self.setheading(360 - self.heading())

        # Move forward
        self.forward(5)
        
        # Reregister a timer for 100 milliseconds in the future.
        turtle.ontimer(self.move_one_step, speed)
    
    

def slow():
    global speed
    speed=speed+50
    
def fast():
    global speed
    if speed>=50:
        speed=speed-50




all_turtles = []

def place_turtle(x, y):
    newT = AnimatedTurtle(x, y, 500, 500)
    all_turtles.append(newT)


# Initialize turtle world
turtle.setworldcoordinates(0,0,500,500)

turtle.onkey(fast, "f")
turtle.onkey(slow, "s")

# Attach the mouse event
turtle.onscreenclick(place_turtle)

turtle.listen()

# Start the tutrle event loop
turtle.mainloop()