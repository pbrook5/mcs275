import turtle

# Event handlers.

def fwd():
    turtle.forward(5)

def bkwd():
    turtle.backward(5)

def left():
    turtle.left(10)

def right():
    turtle.right(10)

def place_circle(x, y):
    newT = turtle.Turtle()
    newT.up()
    newT.speed(0)
    newT.goto(x,y)
    newT.color("red")
    newT.shape("circle")
    newT.pensize(3)
    newT.stamp()
    
def place_circle2(x, y):
    turtle.up()
    turtle.speed(0)
    turtle.goto(x,y)
    turtle.down()

def quit():
    turtle.bye()

def reset():
    turtle.goto(0,0)
    turtle.clear()
    
turtle.pensize(2)
turtle.speed(0) # speed 0 = no animantion, just jump the turtle

# Attach the handlers for keyboard events.
turtle.onkey(quit, "q")
turtle.onkey(reset,"r")
turtle.onkey(fwd, "Up")
turtle.onkey(bkwd, "Down")
turtle.onkey(left, "Left")
turtle.onkey(right, "Right")

# now the mouse event
turtle.onscreenclick(place_circle2)


turtle.listen()

# Start the turtle event loop
turtle.mainloop()