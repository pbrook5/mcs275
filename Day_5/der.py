# Derangement

def memoize(f):
    """
    Input: a function of a single argument
    Output: a function which memoizes the input function, storing computed values in a dictionary
            to speed up successive calls.
    """
    memo = {}
    def wrapped_f(n):

        # Check if the value has been computed already
        if n in memo:
            return memo[n]

        # If not compute it
        ret = f(n)

        # Store it in the memo and return
        memo[n] = ret
        return ret

    return wrapped_f
    

@memoize
def der(n):
	if n == 0:
		return 1
	elif n == 1:
		return 0
	else:
		return (n-1)*(der(n-1)+der(n-2))
import cProfile
cProfile.run("der(25)")


