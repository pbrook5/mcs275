def is_nested(str):
    stack = []
    count = 0
    for c in str:
        if c == "{" or c == "(" or c == "[" or c == "<":
            stack.append(c) # this is push
        elif c == "}":
            if len(stack)>count:
                count=len(stack)
            x = stack.pop()
            if x != "{":
                return False
                print "expected { at character index " len(stack) ", but received " c "instead" 
        elif c == "]":
            if len(stack)>count:
                count=len(stack)
            x = stack.pop()
            if x != "[":
                return False
                print "expected [ at character index " len(stack) ", but received " c "instead" 
        elif c == ")":
            if len(stack)>count:
                count=len(stack)
            x = stack.pop()
            if x != "(":
                return False
                print "expected ( at character index " len(stack) ", but received " c "instead" 
        elif c == ">":
            if len(stack)>count:
                count=len(stack)
            x = stack.pop()
            if x != "<":
                return False
                print "expected < at character index " len(stack) ", but received " c "instead" 
    return True
    print "The deepest nesting that occurred was " count "deep"

print is_nested("{[()}")
print is_nested("{[]({})}")
print is_nested("{[]<{})}")
