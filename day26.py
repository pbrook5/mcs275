mport asyncio
from aiohttp import web

##########################################################
# Some code copied from Day 5 to compute Fibonacci numbers
##########################################################

def memoize(f):
    memo = {}
    def wrapped_f(n):
        if n in memo:
            return memo[n]
        ret = f(n)
        memo[n] = ret
        return ret
    return wrapped_f

@memoize
def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n-1) + fib(n-2)

##########################################################
# The handlers
##########################################################

@asyncio.coroutine
def hello(request):
    return web.Response(body=b"<html><body><p>Compute the <a href='/fib/145'>145th Fibonacci number</a></p></body></html>")

@asyncio.coroutine
def fib_document(request):
    # Find the number given in the path
    n = int(request.match_info['num'])
    # Compute the Fibonacci number
    f = fib(n)
    # Create the resulting document.  We could use HTML here as well.
    t = "The %i Fibonacci number is %i" % (n, f)
    # Send the document
    return web.Response(body=bytes(t, 'utf-8'))

app = web.Application()
app.router.add_route('GET', '/', hello)
app.router.add_route('GET', '/fib/{num}', fib_document)

##########################################################
# The event loop
##########################################################

loop = asyncio.get_event_loop()
f = loop.create_server(app.make_handler(), '0.0.0.0', 8080)
srv = loop.run_until_complete(f)
print('serving on', srv.sockets[0].getsockname())
try:
    loop.run_forever()
except KeyboardInterrupt:
    pass